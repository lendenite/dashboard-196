package com.lendenclub.steps;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import com.opencsv.CSVWriter;

public class CheckinAndRerivingDeails {
	
	WebDriver driver;
	List<WebElement> rows_table;
	String element, arr[]=new String[100],columName[]= {"Job Status  ","Job Name","Last Run Time"};
	int rows_count,userNo=1,times=1;
	boolean test=false;
	CSVWriter writer;
	
	public CheckinAndRerivingDeails(WebDriver driver) throws IOException 
	{
		this.driver=driver;
		rows_table = driver.findElements(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div/div/div/div[1]/div/div/div[2]/div[1]/div/table/tbody/tr"));
		rows_count = rows_table.size();	
		System.out.println(times+"nd value of tr is "+rows_count);
		times++;
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  	
		writer=new CSVWriter(new FileWriter("./Resources/DashBoard-196 ["+formatter.format(date)+"].csv"));
		writer.writeNext(columName);
		
		CheckingStatus();
		
		storeDataInCSVFile();
		
		Assert.assertTrue(test);
			
	}

	private void storeDataInCSVFile() throws IOException {

		writer.flush();
	}

	private void CheckingStatus() {
		
		for( int i=1; i<rows_count; i++)
		{
			element = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div/div[1]/div/div/div[2]/div[1]/div/table/tbody/tr["+i+"]/td[3]")).getText();
			
			if( element.equals("FAILED"))
			{
				storeUserData(element,i);	
			}
			
		}
		
		try {
			
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div/div/div/div[1]/div/div/div[2]/div[2]/span[3]")).click();
			rows_table = driver.findElements(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div/div/div/div[1]/div/div/div[2]/div[1]/div/table/tbody/tr"));
			rows_count = rows_table.size();
			System.out.println(times+"nd value of tr is "+rows_count);
			times++;
			if(rows_count!=0)
			{
				CheckingStatus();
			}
			
		}catch(Exception e)
		{}
			
	}

	private void storeUserData(String status ,int i ) 
	{
		arr[0]=String.valueOf(status);
		for (int j=1 ; j<=2; j++)
		{
			element = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div/div[1]/div/div/div[2]/div[1]/div/table/tbody/tr["+i+"]/td["+j+"]")).getText();
			arr[j]  = element;
			
		}
		
		writer.writeNext(arr);
		test=true;
		
//		System.out.println("User no : "+userNo);
//		System.out.println("the status is : "+arr[0]);
//		System.out.println("the job is :"+arr[1]);
//		System.out.println("the date is : "+arr[2]); 
//		System.out.println();
//		userNo++;
		
		
	}
		
		
}

