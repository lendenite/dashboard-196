package com.lendenclub.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.CheckinAndRerivingDeails;
import com.lendenclub.steps.Login;

public class TestExecution extends WebCapability{
	
	WebDriver driver;

	@BeforeTest
	public void openingFireFox()
	{	   
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage()
	{
	  	new Login(driver);
	  	
	}
	
	@Test(priority = 2)
	public void  ChechingJobStatus() throws IOException
	{
	  	new CheckinAndRerivingDeails(driver);
	  	
	}
	
	@AfterTest
	public void CloseBrowser()
	{
		
	 	driver.quit();
	}




}
